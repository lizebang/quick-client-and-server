#ifndef RSA_H
#define RSA_H

#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <string>

#define KEY_LENGTH 1024
#define PUBLIC_EXPONENT 59 // Public exponent should be a prime number.

class RSATool {
public:
    std::string privateKey;
    std::string publicKey;

    RSATool(const int len = KEY_LENGTH);
    ~RSATool();

private:
    RSA* keypair;
};

#endif // RSA_H
