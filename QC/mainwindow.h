#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDateTime>
#include <QFileDialog>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QTimer>
#include <QtNetwork>

#include "rsa.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();
    void startServer();
    void stopServer();
    void generateKey(int);
    void sendMessage();
    void sendFile();
    void saveFile();
    void sendFileReq();
    void sendFileResp(QString&);
    void handlerMessage(QString&);

private slots:
    void timerUpdate();
    void closeConnection();
    void readMessage();
    void openFile();
    void on_actionStartServer_triggered();
    void on_actioneStopServer_triggered();
    void on_actioneGenerateKey_triggered();
    void on_actionSendMessage_triggered();
    void on_actionSendFile_triggered();
    void on_pushButtonSendMessage_clicked();
    void on_pushButtonSendFile_clicked();

private:
    Ui::MainWindow* ui;
    QLabel* permanent;
    QTcpSocket* tcpSocket;
    RSATool* rsa;

    std::vector<QMetaObject::Connection> tcpSocketConnections;

    QString sendAbsFilename;
    QString sendFilename;
    QString saveAbsFilename;
    QString saveFilename;
};

#endif // MAINWINDOW_H
