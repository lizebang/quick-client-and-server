#ifndef CONSTANT_H
#define CONSTANT_H

#include <QColor>

namespace Ui {
static const QColor NotifyColor(240, 63, 36);
static const QColor SystemColor(39, 117, 182);
static const QColor HostColor(32, 161, 98);
static const QColor RemoteColor(71, 75, 76);

static const QString Type("type");
static const QString Data("data");
static const qint8 TypeMessage = 0x01;
static const qint8 TypeFile = 0x10;
static const qint8 TypeFileReq = 0x11;
static const qint8 TypeFileResp = 0x12;
static const QString FileReqYes("yes");
static const QString FileReqNo("no");
}

#endif // CONSTANT_H
