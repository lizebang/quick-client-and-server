#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDateTime>
#include <QFileDialog>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QTimer>
#include <QtNetwork>

#include "rsa.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void timerUpdate();

private slots:
    void initConnection();
    void closeConnection();
    void on_actionStartServer_triggered();
    void on_actioneStopServer_triggered();
    void on_actioneGenerateKey_triggered();

private:
    void startServer();
    void stopServer();
    void generateKey(int);
    void handleMessage(QString&);

private slots:
    void readMessage();
    void on_actionSendMessage_triggered();
    void on_pushButtonSendMessage_clicked();

private:
    void sendMessage();

private slots:
    void openFile();
    void on_actionSendFile_triggered();
    void on_pushButtonSendFile_clicked();

private:
    void sendFile();
    void saveFile();
    void sendFileReq();
    void sendFileResp(QString&);

private:
    Ui::MainWindow* ui;
    QLabel* permanent;

    RSATool* rsa;

    QTcpServer* tcpServer;
    QTcpSocket* tcpSocket;
    QMetaObject::Connection tcpServerConnection;
    std::vector<QMetaObject::Connection> tcpSocketConnections;

    QString sendAbsFilename;
    QString sendFilename;
    QString saveAbsFilename;
    QString saveFilename;
};

#endif // MAINWINDOW_H
