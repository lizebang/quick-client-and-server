#ifndef SOCKET_H
#define SOCKET_H

#include <QtNetwork>

namespace ZIMU {
class Socket;
}

class Socket : public QTcpSocket {
 public:
  explicit Socket(QTcpSocket *parent = nullptr);
  ~Socket();
  void zConnect(const QObject *sender, const char *signal,
                const QObject *receiver, const char *member,
                Qt::ConnectionType = Qt::AutoConnection);
  void zConnect(const QObject *sender, const QMetaMethod &signal,
                const QObject *receiver, const QMetaMethod &method,
                Qt::ConnectionType type = Qt::AutoConnection);
  void zConnect(const QObject *sender, const char *signal, const char *member,
                Qt::ConnectionType type = Qt::AutoConnection);

 private:
  std::vector<QMetaObject::Connection> zConnections;
};

#endif  // SOCKET_H
