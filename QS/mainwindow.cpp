#include "mainwindow.h"
#include "constant.h"
#include "ui_mainwindow.h"

QString buildMessage(QString&, QColor, QColor);
QString stringToHtml(QString, QColor);

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    permanent = new QLabel(this);
    tcpServer = nullptr;
    tcpSocket = nullptr;
    rsa = nullptr;

    Qt::WindowFlags flags = nullptr;
    QTimer* timer = new QTimer(this);

    flags |= Qt::WindowMinimizeButtonHint;
    setWindowFlags(flags);
    setFixedSize(760, 520);
    setWindowTitle("Quick Server");
    setWindowIcon(QIcon(":/images/emoji/images/emoji/flaticon.png"));

    ui->statusBar->addPermanentWidget(permanent);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    timer->start(500);

    ui->lineEditServerIP->setReadOnly(true);
    ui->lineEditServerIP->setAlignment(Qt::AlignHCenter);
    ui->lineEditPort->setReadOnly(true);
    ui->lineEditPort->setAlignment(Qt::AlignHCenter);
    ui->textBrowserMessage->setReadOnly(true);
    ui->textBrowserPriKey->setReadOnly(true);
    ui->textBrowserPubKey->setReadOnly(true);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete permanent;
    delete rsa;
}

void MainWindow::startServer()
{
    std::string ipAddress;
    auto allAddresses = QNetworkInterface::allAddresses();

    for (auto&& item : allAddresses) {
        if (item.protocol() == QAbstractSocket::IPv4Protocol && item != QHostAddress::Null
            && item != QHostAddress::LocalHost) {
            ipAddress = item.toString().toStdString();
        }
    }

    if (ipAddress.length() != 0) {
        tcpServer = new QTcpServer();
        if (!tcpServer->listen(QHostAddress(ipAddress.c_str()))) {
            QMessageBox::critical(this, tr("启动服务失败"), tcpServer->errorString());
            close();
        }
        tcpServerConnection = connect(tcpServer, SIGNAL(newConnection()), this, SLOT(initConnection()));

        ui->lineEditServerIP->setText(tr(ipAddress.c_str()));
        ui->lineEditPort->setText(tr(std::to_string(tcpServer->serverPort()).c_str()));
        ui->actionStartServer->setEnabled(false);
    } else {
        QMessageBox::warning(this, tr("启动服务失败"), tr("请检查网络连接是否正确，或切换网络环境后重试。"));
    }
}

void MainWindow::stopServer()
{
    sendAbsFilename.clear();

    if (!ui->actioneGenerateKey->isEnabled()) {
        ui->textBrowserPriKey->clear();
        ui->textBrowserPubKey->clear();
        ui->actioneGenerateKey->setEnabled(true);
        ui->radioButtonKey1024->clearFocus();
        ui->radioButtonKey1024->setEnabled(true);
        ui->radioButtonKey2048->clearFocus();
        ui->radioButtonKey2048->setEnabled(true);
        ui->radioButtonKey4096->clearFocus();
        ui->radioButtonKey4096->setEnabled(true);
    }

    if (!ui->actionStartServer->isEnabled()) {
        closeConnection();
        disconnect(tcpServerConnection);
        tcpServer->close();
        tcpServer = nullptr;

        ui->lineEditServerIP->clear();
        ui->lineEditPort->clear();
        ui->actionStartServer->setEnabled(true);
    } else {
        QMessageBox::warning(this, tr("停止服务失败"), tr("停止服务失败，服务尚未启动。"));
    }
}

void MainWindow::generateKey(int len)
{
    rsa = new RSATool(len);

    ui->textBrowserPriKey->setText(tr(rsa->privateKey.c_str()));
    ui->textBrowserPubKey->setText(tr(rsa->publicKey.c_str()));
    ui->actioneGenerateKey->setEnabled(false);
    ui->radioButtonKey1024->setEnabled(false);
    ui->radioButtonKey2048->setEnabled(false);
    ui->radioButtonKey4096->setEnabled(false);
}

void MainWindow::sendMessage()
{
    QByteArray block;
    QJsonObject json;
    QJsonDocument document;
    QDataStream out(&block, QIODevice::WriteOnly);
    QString hostMsg = ui->textEditMessage->toPlainText().toHtmlEscaped();
    QString remoteMsg = buildMessage(hostMsg, Ui::HostColor, Ui::RemoteColor);
    json.insert(Ui::Type, Ui::TypeMessage);
    json.insert(Ui::Data, remoteMsg);
    document.setObject(json);

    out.setVersion(QDataStream::Qt_5_12);
    out << static_cast<quint16>(0);
    out << QString(document.toJson(QJsonDocument::Compact));
    out.device()->seek(0);
    out << static_cast<quint16>(static_cast<unsigned>(block.size()) - sizeof(quint16));

    tcpSocket->write(block);

    ui->textBrowserMessage->append(hostMsg);
    ui->textEditMessage->clear();

    ui->statusBar->showMessage("消息发送成功！", 1000);
}

void MainWindow::sendFile()
{
    qint64 totalBytes = 0;
    QByteArray outBlock;
    qint64 bytesToWrite;

    QFile* localFile = new QFile(sendAbsFilename);
    if (!localFile->open(QFile::ReadOnly)) {
        sendAbsFilename.clear();
        QMessageBox::critical(this, tr("发送文件失败"), tr("打开文件 %1 失败").arg(sendAbsFilename));
        return;
    }

    totalBytes = localFile->size();
    QDataStream sendOut(&outBlock, QIODevice::WriteOnly);
    sendOut.setVersion(QDataStream::Qt_5_12);
    sendOut << qint64(0) << qint64(0) << sendFilename;
    totalBytes += outBlock.size();
    sendOut.device()->seek(0);
    sendOut << totalBytes << qint64((static_cast<unsigned>(outBlock.size()) - sizeof(qint64) * 2));

    bytesToWrite = totalBytes - tcpSocket->write(outBlock);
    outBlock.resize(0);
}

void MainWindow::saveFile()
{
}

void MainWindow::sendFileReq()
{
    QByteArray block;
    QJsonObject json;
    QJsonDocument document;
    QDataStream out(&block, QIODevice::WriteOnly);

    openFile();
    json.insert(Ui::Type, Ui::TypeFileReq);
    json.insert(Ui::Data, sendFilename);
    document.setObject(json);

    out.setVersion(QDataStream::Qt_5_12);
    out << static_cast<quint16>(0);
    out << QString(document.toJson(QJsonDocument::Compact));
    out.device()->seek(0);
    out << static_cast<quint16>(static_cast<unsigned>(block.size()) - sizeof(quint16));

    tcpSocket->write(block);

    ui->statusBar->showMessage("等待对方接收...");
}

void MainWindow::sendFileResp(QString& message)
{
    QMessageBox box;
    QByteArray block;
    QJsonObject json;
    QJsonDocument document;
    QDataStream out(&block, QIODevice::WriteOnly);
    QString choice;
    json.insert(Ui::Type, Ui::TypeFileResp);

    box.setWindowTitle(tr("保存文件"));
    box.setIcon(QMessageBox::Information);
    box.setText(tr("对方发送文件 %1，请选择是否保存？").arg(message));
    QPushButton* yesButton = box.addButton(tr("是"),
        QMessageBox::YesRole);
    box.addButton(tr("否(&N)"), QMessageBox::NoRole);
    box.exec();

    if (box.clickedButton() == yesButton) {
        choice = Ui::FileReqYes;
    } else {
        choice = Ui::FileReqNo;
    }
    json.insert(Ui::Data, choice);
    document.setObject(json);

    if (choice == Ui::FileReqYes) {
    }

    out.setVersion(QDataStream::Qt_5_12);
    out << static_cast<quint16>(0);
    out << QString(document.toJson(QJsonDocument::Compact));
    out.device()->seek(0);
    out << static_cast<quint16>(static_cast<unsigned>(block.size()) - sizeof(quint16));

    tcpSocket->write(block);

    ui->statusBar->clearMessage();
}

void MainWindow::handleMessage(QString& message)
{
    QJsonParseError error;
    QJsonDocument doucment = QJsonDocument::fromJson(message.toUtf8(), &error);

    if (error.error == QJsonParseError::NoError && doucment.isObject()) {
        QJsonObject json = doucment.object();
        if (json.contains(Ui::Type)) {
            qint8 type = static_cast<qint8>(json.take(Ui::Type).toInt());
            QString data = json.take(Ui::Data).toString();
            switch (type) {
            case Ui::TypeMessage:
                ui->textBrowserMessage->append(data);
                break;
            case Ui::TypeFile:
                break;
            case Ui::TypeFileReq:
                sendFileResp(data);
                break;
            case Ui::TypeFileResp:
                if (data == Ui::FileReqYes) {
                    sendFile();
                } else {
                    sendAbsFilename.clear();
                    QMessageBox::information(this, tr("文件发送请求失败"), tr("对方拒接了你的文件发送请求"));
                }
                break;
            }
        }
    }
}

void MainWindow::timerUpdate()
{
    QDateTime time = QDateTime::currentDateTime();
    QString str = time.toString("yyyy-MM-dd hh:mm:ss dddd");
    permanent->setText(str);
}

void MainWindow::initConnection()
{
    tcpSocket = tcpServer->nextPendingConnection();
    tcpSocketConnections.push_back(connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage())));
    tcpSocketConnections.push_back(connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(closeConnection())));

    ui->textBrowserMessage->append(stringToHtml(tr("客户端接入成功！"), Ui::NotifyColor));
}

void MainWindow::closeConnection()
{
    if (tcpSocket != nullptr) {
        for (auto&& item : tcpSocketConnections) {
            disconnect(item);
        }
        tcpSocketConnections.clear();

        tcpSocket->close();
        tcpSocket = nullptr;
        ui->textBrowserMessage->append(stringToHtml(tr("客户端已断开连接！"), Ui::NotifyColor));
    }
}

void MainWindow::readMessage()
{
    if (tcpSocket != nullptr) {
        qint16 blockSize = 0;
        QString message;

        QDataStream in(tcpSocket);
        in.setVersion(QDataStream::Qt_5_12);

        if (blockSize == 0) {
            if (tcpSocket->bytesAvailable() < static_cast<qint64>(sizeof(quint16)))
                return;
            in >> blockSize;
        }
        if (tcpSocket->bytesAvailable() < blockSize)
            return;

        in >> message;
        handleMessage(message);
    }
}

void MainWindow::openFile()
{
    sendAbsFilename = QFileDialog::getOpenFileName(this, "File Selector", "/");

    if (!sendAbsFilename.isEmpty()) {
        sendFilename = QDir(sendAbsFilename).dirName();
        ui->statusBar->showMessage(tr("打开文件 %1 成功！")
                                       .arg(sendAbsFilename));
    }
}

void MainWindow::on_actionStartServer_triggered()
{
    startServer();
}

void MainWindow::on_actioneStopServer_triggered()
{
    stopServer();
}

void MainWindow::on_actioneGenerateKey_triggered()
{
    int keyLen = 0;

    if (ui->radioButtonKey1024->isChecked()) {
        keyLen = 1024;
    } else if (ui->radioButtonKey2048->isChecked()) {
        keyLen = 2048;
    } else if (ui->radioButtonKey4096->isChecked()) {
        keyLen = 4096;
    } else {
        QMessageBox::warning(this, tr("生成密钥失败"), tr("请选择密钥长度，1024、2048或4096位。"));
    }

    if (keyLen != 0) {
        generateKey(keyLen);
    }
}

void MainWindow::on_actionSendMessage_triggered()
{
    if (tcpSocket != nullptr && ui->textEditMessage->document()->isModified()) {
        sendMessage();
    }
}

void MainWindow::on_actionSendFile_triggered()
{
    if (tcpSocket != nullptr) {
        if (sendAbsFilename.isEmpty()) {
            sendFileReq();
        } else {
            QMessageBox::warning(this, tr("文件发送失败"), tr("请等待上一个文件发送完毕。"));
        }
    }
}

void MainWindow::on_pushButtonSendMessage_clicked()
{
    on_actionSendMessage_triggered();
}

void MainWindow::on_pushButtonSendFile_clicked()
{
    on_actionSendFile_triggered();
}

QString buildMessage(QString& str, QColor host, QColor remote)
{
    QString tmp(str);
    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    time = stringToHtml(time, Ui::SystemColor);

    str = QString("%1<br>%2").arg(time).arg(stringToHtml(str, host));
    return QString("%1<br>%2").arg(time).arg(stringToHtml(tmp, remote));
}

QString stringToHtml(QString str, QColor clr)
{
    QByteArray array;
    array.append(static_cast<char>(clr.red()));
    array.append(static_cast<char>(clr.green()));
    array.append(static_cast<char>(clr.blue()));
    QString strC(array.toHex());
    return QString("<span style=\" color:#%1;\">%2</span>").arg(strC).arg(str);
}
