#include "socket.h"

Socket::Socket(QTcpSocket *parent) : QTcpSocket(parent) {}

Socket::~Socket() {
  for (auto &&item : zConnections) {
    disconnect(item);
  }
  zConnections.clear();
}

void Socket::zConnect(const QObject *sender, const char *signal,
                      const QObject *receiver, const char *member,
                      Qt::ConnectionType) {
  zConnections.push_back(connect(sender, signal, receiver, member));
}

void Socket::zConnect(const QObject *sender, const QMetaMethod &signal,
                      const QObject *receiver, const QMetaMethod &method,
                      Qt::ConnectionType type) {
  zConnections.push_back(connect(sender, signal, receiver, method, type));
}

void Socket::zConnect(const QObject *sender, const char *signal,
                      const char *member, Qt::ConnectionType type) {
  zConnections.push_back(connect(sender, signal, member, type));
}
