#include "rsa.h"

RSATool::RSATool(const int len)
{
    size_t priLen;
    size_t pubLen;
    char* priKey = nullptr;
    char* pubKey = nullptr;

    keypair = RSA_generate_key(len, PUBLIC_EXPONENT, nullptr, nullptr);

    BIO* pri = BIO_new(BIO_s_mem());
    BIO* pub = BIO_new(BIO_s_mem());

    PEM_write_bio_RSAPrivateKey(pri, keypair, nullptr, nullptr, 0, nullptr, nullptr);
    PEM_write_bio_RSAPublicKey(pub, keypair);

    priLen = static_cast<size_t>(BIO_pending(pri));
    pubLen = static_cast<size_t>(BIO_pending(pub));

    priKey = static_cast<char*>(malloc(priLen + 1));
    pubKey = static_cast<char*>(malloc(pubLen + 1));

    BIO_read(pri, priKey, static_cast<int>(priLen + 1));
    BIO_read(pub, pubKey, static_cast<int>(pubLen + 1));

    priKey[priLen] = '\0';
    pubKey[pubLen] = '\0';

    privateKey = priKey;
    publicKey = pubKey;

    BIO_free_all(pub);
    BIO_free_all(pri);
    free(priKey);
    free(pubKey);
}

RSATool::~RSATool()
{
    RSA_free(keypair);
}
